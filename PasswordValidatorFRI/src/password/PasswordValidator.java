package password;
/**
 * 
 * @author Luc Laffin 991389277
 * 
 * This class will validate passwords to ensure they are the correct length
 * and contain the correct characters 
 * Assumption: password should not contain any spaces, spaces do not count towards password length
 * Assumption: password cannot be null
 */

public class PasswordValidator {
	
	private static int MIN_LENGTH = 8;
	private static int MIN_DIGITS = 2;
	
	public static boolean hasValidCaseChars(String password) {
		return password.matches(".*[A-Z]+.*") && password.matches(".*[a-z]+.*");
	}
	
	public static boolean hasValidNumDigits(String password) {
		int numDigits = 0;
		for(int i = 0; i < password.length(); i++) {
			if(Character.isDigit(password.charAt(i))) numDigits++;
		}
		return numDigits >= MIN_DIGITS;
	}
	
	
	public static boolean isValidLength(String password) {
		return (password != null && !password.contains(" ") && password.length() >= MIN_LENGTH);
	}
	
}
