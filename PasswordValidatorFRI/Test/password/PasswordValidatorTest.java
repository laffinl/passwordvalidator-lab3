package password;
import password.PasswordValidator;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 * 
 * @author Luc Laffin 991389277
 * 
 * This class will test the methods in the PasswordValidator class to ensure they are
 * returning the correct and expeced values
 *
 */

public class PasswordValidatorTest {
	
	@Test
	public void testHasValidCaseCharsRegular() {
		assertTrue("Invalid case chars", PasswordValidator.hasValidCaseChars("fDeefefDDeefefe"));
	}
	
	@Test
	public void testHasValidCaseCharsException() {
		assertFalse("Invalid case chars", PasswordValidator.hasValidCaseChars("12345"));
	}
	
	@Test
	public void testHasValidCaseCharsExceptionEmpty() {
		assertFalse("Invalid case chars", PasswordValidator.hasValidCaseChars(""));
	}
	
	@Test
	public void testHasValidCaseCharsBoundaryIn() {
		assertTrue("Invalid case chars", PasswordValidator.hasValidCaseChars("aA"));
	}
	
	@Test
	public void testHasValidCaseCharsBoundaryOutUpper() {
		assertFalse("Invalid case chars", PasswordValidator.hasValidCaseChars("A"));
	}
	
	@Test
	public void testHasValidCaseCharsBoundaryOutLower() {
		assertFalse("Invalid case chars", PasswordValidator.hasValidCaseChars("a"));
	}

	@Test
	public void testIsValidLengthRegular() {
		boolean isValid = PasswordValidator.isValidLength("thisisanacceptablepassword");
		assertTrue("Invalid password length", isValid);
	}
	
	@Test
	public void testIsValidLengthExceptional() {
		boolean isValid = PasswordValidator.isValidLength("123");
		assertFalse("Invalid password length", isValid);
	}
	
	@Test
	public void testIsValidLengthBoundaryIn() {
		boolean isValid = PasswordValidator.isValidLength("12345678");
		assertTrue("Invalid password length", isValid);
	}
	
	@Test
	public void testIsValidLengthBoundaryOut() {
		boolean isValid = PasswordValidator.isValidLength("1234567");
		assertFalse("Invalid password length", isValid);
	}
	
	///////
	
	@Test
	public void testHasValidNumDigitsRegular() {
		boolean isValid = PasswordValidator.hasValidNumDigits("hello1234");
		assertTrue("Invalid number of digits", isValid);
	}
	
	@Test
	public void testHasValidNumDigitsExceptional() {
		boolean isValid = PasswordValidator.hasValidNumDigits("passwordpass");
		assertFalse("Invalid number of digits", isValid);
	}
	
	@Test
	public void testHasValidNumDigitsBoundaryIn() {
		boolean isValid = PasswordValidator.hasValidNumDigits("password12");
		assertTrue("Invalid number of digits", isValid);
	}
	
	@Test
	public void testHasValidNumDigitsBoundaryOut() {
		boolean isValid = PasswordValidator.hasValidNumDigits("password1");
		assertFalse("Invalid number of digits", isValid);
	}
	
	
}
